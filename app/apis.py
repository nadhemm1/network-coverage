from flask import Blueprint, request
from flask_cors import CORS

from app.network_coverage_service import get_network_coverage_by_operator
from app.populate_db import seed_db

rest = Blueprint("rest", __name__)

CORS(rest)


@rest.route("/db_setup", methods=["POST"])
def ep_setup_db():
    """
        Endpoint Used the first time the app runs to fill the db with operators based on csv file
    """
    return seed_db(), 200


@rest.route("/network_coverage", methods=["GET"])
def ep_get_network_coverage():
    address = request.args["q"]
    return get_network_coverage_by_operator(address), 200
