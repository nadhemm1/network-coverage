from typing import Dict, List

from app import france_address_api_client
from app.models import NetworkCoverage


def get_network_coverage_by_operator(address: str) -> Dict[str, Dict[str, bool]]:
    # get gps coordinates from address
    city = france_address_api_client.get_city_from_address(address)

    # get all coverages for city
    available_network_coverages = NetworkCoverage.query.filter_by(
        # TODO: format to lower case
        city=city,
    ).all()

    # format result to required output
    result = _format_output(available_network_coverages)
    return result


def _format_output(available_network_coverages: List[NetworkCoverage]):
    """
        returns coverages in the following format:
        {
            "orange": {"2G": true, "3G": true, "4G": false},
            "SFR": {"2G": true, "3G": true, "4G": true}
        }
    """
    return {
        coverage.network_operator.brand: {
            "2G": coverage.has_2g_coverage,
            "3G": coverage.has_3g_coverage,
            "4G": coverage.has_4g_coverage,
        }
        for coverage in available_network_coverages
    }
