from urllib.parse import urljoin

import requests
from flask import current_app


def get_city_from_address(query: str) -> str:
    features = requests.get(
        _url("search"), params={"q": query, "autocomplete": 1},
    ).json()["features"]
    best_score_feature = max(
        features, key=lambda feature: feature["properties"]["score"],
    )
    return best_score_feature["properties"]["city"]


def backfill_city_from_coordinates_by_csv_file(input_csv_file_path: str) -> str:
    files = {"data": (input_csv_file_path, open(input_csv_file_path, "rb"))}
    return requests.post(
        _url("reverse/csv"), data={"result_columns": ["result_city"]}, files=files,
    ).text


def _url(endpoint: str) -> str:
    return urljoin(current_app.config["ADDRESS_API_URL"], endpoint)
