import os

from flask import Flask
from flask_cors import CORS

from app.config import Config, config
from app.models import db


def create_app(env):
    app = Flask(__name__)

    CORS(app, origins="*", supports_credentials=True)

    config_name = env
    app.config.from_object(config[config_name])

    db.init_app(app)

    from app.apis import rest as rest_blueprint

    from . import models

    app.register_blueprint(rest_blueprint, url_prefix="/rest")

    return app


app = create_app(os.getenv("FLASK_CONFIG") or "local")
