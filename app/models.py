from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class NetworkOperator(db.Model):
    __tablename__ = "network_operators"

    id = db.Column(db.Integer, primary_key=True)
    mcc_code = db.Column(db.String(255))
    mnc_code = db.Column(db.String(255))
    brand = db.Column(db.String(255), nullable=True)
    operator = db.Column(db.String(255), nullable=True)


class NetworkCoverage(db.Model):
    __tablename__ = "network_coverages"

    id = db.Column(db.Integer, primary_key=True)
    x_coordinate = db.Column(db.Integer)
    y_coordinate = db.Column(db.Integer)

    has_2g_coverage = db.Column(db.Boolean)
    has_3g_coverage = db.Column(db.Boolean)
    has_4g_coverage = db.Column(db.Boolean)

    # we assume that this solution is only available for France, when deployed to other countries
    network_operator_id = db.Column(db.Integer, db.ForeignKey("network_operators.id"))
    network_operator = db.relationship("NetworkOperator")

    # computed based on x,y coordinates, indexed field as most queries will be filtered based on the city
    city = db.Column(db.String(255), nullable=True, index=True)
