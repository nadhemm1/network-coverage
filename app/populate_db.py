import csv
from typing import Optional

import pandas as pd
from flask import current_app

from app import db, france_address_api_client
from app.exceptions import InvalidOperator
from app.helpers import lamber93_to_gps
from app.models import NetworkCoverage, NetworkOperator

DEFAULT_NETWORKS_OPERATORS_CSV_PATH = "app/csv_files/operators.csv"  # exported from wikipedia see: https://fr.wikipedia.org/wiki/Mobile_Network_Code
DEFAULT_NETWORKS_COVERAGES_CSV_PATH = (
    "app/csv_files/2018_01_Sites_mobiles_2G_3G_4G_France_metropolitaine_L93.csv"
)


def seed_db():
    _seed_networks_operators()
    _seed_networks_coverages()


def _seed_networks_operators(
    input_file_path: str = DEFAULT_NETWORKS_OPERATORS_CSV_PATH,
):
    """
        seeds network operators table from csv file, provided from wikipedia
    """
    seed_db_from_input_file(input_file_path, "network_operators")


def _seed_networks_coverages(
    input_file_path: str = DEFAULT_NETWORKS_COVERAGES_CSV_PATH,
):
    """
        seed network coverages table with input from csv file
    """
    csv_with_lon_lat_file_path = _add_lon_lat_columns_to_csv_file(input_file_path)
    csv_with_cities_file_path = _add_city_column_to_csv_file(csv_with_lon_lat_file_path)
    seed_db_from_input_file(csv_with_cities_file_path, "network_coverages")


def seed_db_from_input_file(input_file_path: str, table: str):
    """
        function used to parse a csv and fill table using corresponding method
    """
    persist_object_function = {
        # pick correct fill function
        "network_operators": _persist_network_operator,
        "network_coverages": _persist_network_coverage,
    }[table]
    # read csv and process raws one-by-one
    with open(input_file_path, "r", newline="") as csv_file:
        csv_reader = csv.reader(csv_file)
        # Read and process the header row
        header = next(csv_reader)
        for input in csv_reader:
            try:
                persist_object_function(dict(zip(header, input)))
            except Exception as e:
                # on prod env, this error should be logged somewhere (using sentry, bugsnag..)
                current_app.logger.error(e)


def _persist_network_operator(data):
    network_operator = NetworkOperator(
        mcc_code=data["MCC"],
        mnc_code=data["MNC3"],
        brand=data["Marque"],
        operator=data["Opérateur"],
    )

    db.session.add(network_operator)
    db.session.commit()


def _persist_network_coverage(data):
    network_operator_id = _get_network_operator_id(data["Operateur"])
    network_coverage = NetworkCoverage(
        x_coordinate=data["x"],
        y_coordinate=data["y"],
        has_2g_coverage=data["2G"] == "1",
        has_3g_coverage=data["3G"] == "1",
        has_4g_coverage=data["4G"] == "1",
        network_operator_id=network_operator_id,
        city=data["result_city"],
    )

    db.session.add(network_coverage)
    db.session.commit()


def _get_network_operator_id(operator: str) -> Optional[int]:
    network_operator_code = str(operator)
    if not network_operator_code.startswith("208"):
        raise InvalidOperator

    mnc_code = network_operator_code[len("208") :]
    # We assume all list of operators has already been filled in the db
    network_operator = NetworkOperator.query.filter_by(
        mcc_code="208", mnc_code=mnc_code,
    ).one()
    return network_operator.id


def _add_lon_lat_columns_to_csv_file(input_file_path: str) -> str:
    """
    Creates a new CSV file with 2 new columns for longitude and latitude using Pandas.
    """

    output_file_path = "tmp/lon_lat.csv"

    # Read the input CSV file into a Pandas DataFrame
    df = pd.read_csv(input_file_path, delimiter=";")

    # Convert x and y columns to float
    df["x"] = df["x"].astype(float)
    df["y"] = df["y"].astype(float)

    # Apply the 'lamber93_to_gps' function to each row to calculate lon and lat
    df["lon"], df["lat"] = zip(
        *df.apply(lambda row: lamber93_to_gps(row["x"], row["y"]), axis=1),
    )

    # Write the new DataFrame to the output CSV file
    df.to_csv(output_file_path, index=False)

    return output_file_path


def _add_city_column_to_csv_file(input_file_path: str) -> str:
    """
        creates a new csv file with a new column: result_city
    """

    data = france_address_api_client.backfill_city_from_coordinates_by_csv_file(
        input_file_path,
    )
    csv_with_cities_file_path = "tmp/output_with_cities.csv"
    with open(csv_with_cities_file_path, "w") as file:
        # Open the file in write mode and write the CSV data
        file.write(data)

    return csv_with_cities_file_path
