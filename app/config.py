import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    SQLALCHEMY_DATABASE_URI = (
        "sqlite:///dev.db"  # TODO: we probably need a better db for prod
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    ADDRESS_API_URL = "https://api-adresse.data.gouv.fr"


class TestConfig(Config):
    ENV_TYPE = "test"
    SQLALCHEMY_DATABASE_URI = "sqlite:///test.db"


class LocalConfig(Config):
    ENV_TYPE = "local"


class DevelopmentConfig(Config):
    ENV_TYPE = "development"


class ProductionConfig(Config):
    ENV_TYPE = "production"


config = {
    "development": DevelopmentConfig,
    "production": ProductionConfig,
    "local": LocalConfig,
    "test": TestConfig,
}
