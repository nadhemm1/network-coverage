import responses
from app.models import NetworkCoverage, NetworkOperator
from app.populate_db import _seed_networks_coverages, _seed_networks_operators
from flask import current_app

from tests.config import *  # noqa

MOCKED_OPERATORS = """MCC,MNC3,Marque,Opérateur
208,01,Orange,Orange
208,02,Orange,Orange
208,03,MobiquiThings,MobiquiThings
"""

MOCKED_COVERAGES = """Operateur;x;y;2G;3G;4G
20801;102980;6847973;1;1;0
20802;103113;6848661;1;1;0
20803;103114;6848664;1;1;1
"""


def test_populate_db(requests_mock):
    _test_seed_network_operators()
    _test_seed_network_coverages(requests_mock)


def _test_seed_network_operators():
    # mock csv file
    file_path = "tmp/mocked_operators.csv"
    _created_mock_csv_file(file_path, MOCKED_OPERATORS)
    _seed_networks_operators(file_path)

    assert len(NetworkOperator.query.all()) == 3
    expected_db_content = [
        ("Orange", "Orange", "208", "01"),
        ("Orange", "Orange", "208", "02"),
        ("MobiquiThings", "MobiquiThings", "208", "03"),
    ]
    assert [
        (
            network_operator.brand,
            network_operator.operator,
            network_operator.mcc_code,
            network_operator.mnc_code,
        )
        for network_operator in NetworkOperator.query.all()
    ] == expected_db_content


def _test_seed_network_coverages(requests_mock):
    # mock address_api rest call
    requests_mock.add(
        responses.POST,
        f"{current_app.config['ADDRESS_API_URL']}/reverse/csv",
        status=200,
        body=b"Operateur,x,y,2G,3G,4G,lon,lat,result_city\r\n20801,102980.0,6847973.0,1,1,0,-5.08885611530134,48.45657455882989,Ouessant\r\n20802,103113.0,6848661.0,1,1,0,-5.08885611530134,48.45657455882989,Ouessant\r\n20803,103114.0,6848664.0,1,1,1,-5.08885611530134,48.45657455882989,Ouessant\r\n",
    )

    # We assume that operators are already created in the previous code
    file_path = "tmp/mocked_coverages.csv"
    _created_mock_csv_file(file_path, MOCKED_COVERAGES)
    _seed_networks_coverages(file_path)

    assert len(NetworkCoverage.query.all()) == 3
    expected_db_content = [
        # x, y, has_2g, has_3g, has_4g
        (102980, 6847973, True, True, False),
        (103113, 6848661, True, True, False),
        (103114, 6848664, True, True, True),
    ]
    assert [
        (
            network_coverage.x_coordinate,
            network_coverage.y_coordinate,
            network_coverage.has_2g_coverage,
            network_coverage.has_3g_coverage,
            network_coverage.has_4g_coverage,
        )
        for network_coverage in NetworkCoverage.query.all()
    ] == expected_db_content


def _created_mock_csv_file(file_path: str, content: str):
    with open(file_path, "w") as file:
        # Open the file in write mode and write the CSV data
        file.write(content)
