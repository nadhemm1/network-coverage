import factory
from app.models import NetworkCoverage, NetworkOperator, db
from sqlalchemy.orm import scoped_session

session_factory = scoped_session(lambda: db.session, scopefunc=lambda: db.session)


class NetworkCoverageFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = NetworkCoverage
        sqlalchemy_session = session_factory
        sqlalchemy_session_persistence = "commit"

    id = factory.Sequence(lambda n: n + 1)
    x_coordinate = factory.Faker("pyint")
    y_coordinate = factory.Faker("pyint")
    has_2g_coverage = False
    has_3g_coverage = False
    has_4g_coverage = False
    network_operator_id = 1


class NetworkOperatorFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = NetworkOperator
        sqlalchemy_session = session_factory
        sqlalchemy_session_persistence = "commit"

    id = factory.Sequence(lambda n: n + 1)
    mcc_code = "208"
    mnc_code = factory.Faker("bothify", text="#####")
