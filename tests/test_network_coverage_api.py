import re

import responses
from flask import current_app

from tests.config import *  # noqa
from tests.factories import NetworkCoverageFactory, NetworkOperatorFactory


def test_get_network_coverage(test_client, requests_mock):
    # mock address_api rest call
    requests_mock.add(
        responses.GET,
        re.compile(f"{current_app.config['ADDRESS_API_URL']}/search\?q=.*"),
        status=200,
        json={
            "features": [
                {"properties": {"city": "Paris", "score": 0.6}},
                {"properties": {"city": "Saint-Ouen-sur-Seine", "score": 0.5}},
            ],
        },
    )

    for brand, city, has_2g_coverage, has_3g_coverage, has_4g_coverage in [
        ("orange", "Paris", True, True, False),
        ("SFR", "Paris", True, True, True),
        ("Noise brand", "Marseille", True, True, True),
    ]:
        network_operator_1 = NetworkOperatorFactory(brand=brand)
        NetworkCoverageFactory(
            city=city,
            network_operator_id=network_operator_1.id,
            has_2g_coverage=has_2g_coverage,
            has_3g_coverage=has_3g_coverage,
            has_4g_coverage=has_4g_coverage,
        )

    response = test_client.get("rest/network_coverage?q=42+rue+papernest+75011+Paris")
    assert response.status_code == 200

    assert response.json == {
        "orange": {"2G": True, "3G": True, "4G": False},
        "SFR": {"2G": True, "3G": True, "4G": True},
    }
    # address api REST call fired
    assert requests_mock.assert_all_requests_are_fired
