# Network Coverage Service
This service is used to provide hints with the network coverage for a specific location.

# Development
**Requirements**

- Python 3.9
- Pipenv: `brew install pipenv`

**Setup**

- Create the environment file: `cp .env.example .env`
- Install dependencies: `pipenv install --dev`
- Setup a db database the way you prefer, in this poc context we will use sqlite, it's already created so no action needed you can move to the next step. Your dev db is under instance/dev.db, objects are left intact 
- Note: CI/CD pipelines and db migrations are also out of the scope of this poc.

**Launch**

- Enable the virtual environment: `pipenv shell`
- `flask run` to launch the 🚀

**Usage**:
Note: If you are running the project for the first time, you probably want to prefill the db tables from csv files
to do so, you need to call /db_setup API:

```shell
curl -X POST http://localhost:5000/rest/db_setup
```
Once your db is ready (you can check it under instance/dev.db), you can call the API to fetch network coverage:
```shell
curl -X GET --url 'http://localhost:5000/rest/network_coverage?q=42%2Brue%2Bpapernest%2B75011%2BParis'
```
**Tests**

You can run the tests using: `pipenv run pytest`
For this poc we added 2 sets of tests:
- `test_populate_db`
- `test_network_coverage_api`

**Author**
- Nadhem